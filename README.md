# Readme-file for Web application version of Varastonhallinta
Varastonhallinta-sovellus vaatii toimiakseen Nodejs:n. Asenna Nodejs ja suorita nodella server.js-tiedosto tästä paketista.

'node server.js'

# Vaatimukset

Nodejs (v0.10.24) - Lataa ja asenna osoitteesta: http://nodejs.org
npm (v0.10.24)- Paketinhallinta Nodejs:lle. 

# Asennusohjeita

 - Installation tutorial for Nodejs on different OS's: http://howtonode.org/how-to-install-nodejs 
 - Install npm (package manager for Nodejs) - http://howtonode.org/introduction-to-npm

# NPM kirjastot ja lisäosat

Seuraavat kirjastot vaaditaan Varastonhallinta-sovelluksen käyttämiseen:

  - npm install execSync
  - npm install websocket
  - npm install url
  - npm install path

# Tiedossa olevat ongelmat ja virheet

- Tietokannan tyhjentämisen jälkeen taulukko näyttää database-tiedostossa olevan newline-merkin tyhjänä rivinä. Tämän jälkeen tiedostoja lisättäessä tyhjä newline-rivi jää edelleen näkyviin. Ei vaadi suuria muutoksia, vain funktion joka tarkistaa onko tiedostossa tyhjiä rivejä, ja jos on niin poistetaan.

- Yksittäisten rivien poistaminen ei ole mahdollista tällä hetkellä muuten kuin käsin poistaen database-tiedostosta. 

- Toiminnallisuuksia olisi kiva olla enemmän.