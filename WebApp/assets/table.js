
//------------------------------------------------
// This javascript-file holds the table functionalities 
// for Varastonhallinta. 
//
// Created by Juuso Karlström in 2013
//------------------------------------------------

var table = document.getElementById('databaseTable');
var tableBody = document.createElement('tbody');

var lineCounter = 1; 	// Counter for table rows

function addNewRow(id, nimi, sijainti, saldo, valmistaja, hinta) {

	if ( nimi == "null") {
		// empty database, doing nothing
	} else 
	var row = tableBody.insertRow(-1);	// Add new row
	row.setAttribute("id","row" + lineCounter);

	// Create cells for table
	countCell = row.insertCell(0)
	idCell = row.insertCell(1)			
	nimiCell = row.insertCell(2)		
	sijaintiCell = row.insertCell(3)
	saldoCell = row.insertCell(4)
	valmistajaCell = row.insertCell(5)
	hintaCell = row.insertCell(6)

	// LineCounter cell values
	countCell.innerHTML = "<strong> " + lineCounter + "</strong>";
	countCell.setAttribute("id", "countCell" + lineCounter);
	
	// Append content to cell
	idCell.textContent = id
	nimiCell.textContent = nimi
	sijaintiCell.textContent = sijainti

	saldoCell.textContent = saldo
	 if (saldo<0) {
	 	row.className = "danger"
	 }

	 if (saldo==0) {
	 	row.className = "warning"
	 }
	 
	valmistajaCell.textContent = valmistaja
	hintaCell.textContent = hinta

	tableBody.appendChild(row)		// Attach row to tableBody
	table.appendChild(tableBody)	// Attach tablebody to table

	lineCounter++	// Increment lineCounter value everytime addNewRow() is called

}

var riviArray = []

function createSampleContent() {

	// Joku array, jonka läpi käydään ja luetaan nimet jne.
	// sample-mössöä, ei oikeaa tietokanta-dataa
	for (i=0; i<2; i++) {
		var temp = i;
		var rivi = rivi + temp;
		console.log("Rivi:" + rivi);
	}
}

// Websocket initialization

var websocket;

function connect() {

	  //open socket
	  if ("WebSocket" in window){
	    websocket = new WebSocket("ws://127.0.0.1:8080/", "echo-protocol");
	  
	    //attach event handlers
	    websocket.onopen = onOpen;
	    websocket.onclose = onClose;

	    websocket.onmessage = receiveOutput;
	    websocket.onerror = onError;
	    }
	    else {
	      alert("WebSockets not supported on your browser.");
	    } // end if

} // end connect

function init(){
} // end init

function onOpen(evt){
	console.log("Websocket initialized")
	// funkkari datan lukemiseen tietokannasta
	updateTable()
} // end onOpen

function onClose(evt){
	closeSocket()
} // end onClose;

function onError(evt){
	window.alert("Error establishing WebSocket-connection to server" + evt.data)
} // end onError

function closeSocket() {
	websocket.close()
	console.log("WebSocket closed");
}

function receiveOutput(evt) {
	// evt.data is the data received from server
	output = evt.data;
	output.toString();

	// Split received data per newline
	var row = output.split(/\n/);
	console.log("Eka rivi: " + row[0])

	if (row[0] == "\n") {
		// Empty database file
		console.log("Empty database file: " + row[0].split(","))
		addNewRow("null", "null", "null", "null", "null", "null")
	} else {
		// File not empty
		console.log("File not empty")
		// Print rows one by one
		for (var i=0;i<row.length;i++) {
			var rowColumns = row[i].split(",")

			// rowColumns[0] = ID
			// rowColumns[1] = Nimi
			// rowColumns[2] = Sijainti
			// rowColumns[3] = Saldo
			// rowColumns[4] = Valmistaja
			// rowColumns[5] = Hinta

			addNewRow(rowColumns[0],rowColumns[1], rowColumns[2], rowColumns[3], rowColumns[4], rowColumns[5])
		}
	}

	// Load function to read lines from table
	readID();

} // END receiveOutput()

function sendCommand() {
	// Tietokantakäskyjen määrittämiset

	var command = "helou"
	websocket.send(command);

	// console.log("command sent to server");
}

function updateTable() {
	var command = "doUpd"
	websocket.send(command);
}

function addProduct() {

	// Store information from user submitted forms to variables
	var id = document.getElementById("inputID").value;
	var name = document.getElementById("inputName").value;
	var location = document.getElementById("inputLocation").value;
	var number = document.getElementById("inputNumber").value;
	var manufacturer = document.getElementById("inputManufacturer").value;
	var price = document.getElementById("inputPrice").value;

	var rowArray = [ id, name, location, number, manufacturer, price ];

	websocket.send("doAdd:" + rowArray);
}

function removeProduct() {
	// foo
	websocket.send("doRem:1234")
}

function readID() {

	var buttonMaker = document.getElementById("rowSelector");

	for(var i=1;i<=lineCounter;i++) {
		// var idCell = document.getElementById("countCell" + i).value;
		// console.log(idCell)

		var rowNumber = document.getElementById("row" + i)

		var btn = document.createElement("button");
		btn.className = "btn btn-default"
		var t=document.createTextNode("ID #" + i);
		btn.appendChild(t);
		buttonMaker.appendChild(btn);
	}

	var deleteButton = document.createElement("button")
	deleteButton.className = "btn btn-warning"
	var deleteText = document.createTextNode("Poista valitut rivit")
	deleteButton.appendChild(deleteText)
	buttonMaker.appendChild(deleteButton)
}

function deleteTable() {

    var conf = confirm("Oletko varma, että haluat poistaa tämän tietokannan? \n\n Poistetun tietokannan tilalle luodaan uusi tyhjä tiedosto.");

    if(conf == true) {
    	 websocket.send("doFlush")
         alert("Tietokanta poistettu.");
         location.reload(true) // Reload the page and table
    }
}

function readRowNumbers() {
	console.log("readRowNumbers kutsuttu: " + lineCounter)
}
