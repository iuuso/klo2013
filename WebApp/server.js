var http = require("http");
var path = require("path");
var url = require("url");
var fs = require("fs");
var WebSocketServer = require('websocket').server;

var spawn = require('child_process').spawn;

// ---------------------------------

// Create dynamic http-server
var server = http.createServer(function(request,response){
        var my_path = url.parse(request.url).pathname;
        var full_path = path.join(process.cwd(),my_path);

        path.exists(full_path,function(exists){
                if(!exists){
            response.writeHeader(404, {"Content-Type": "text/plain"});
                        response.write("404 Not Found\n");
                        response.end();
                }
                else{
                        fs.readFile(full_path, "binary", function(err, file) {
                         if(err) {
                         response.writeHeader(500, {"Content-Type": "text/plain"});
                         response.write(err + "\n");
                         response.end();
                        
                         }
                                 else{
                                        response.writeHeader(200);
                         response.write(file, "binary");
                         response.end();
                                }
    
                        });
                }
        });
})        

//Set server to listen port 8080
server.listen(8080, function(err) {
    console.log("------------------------------")
    console.log('Server listening port '+'8080')
    console.log("Address: http://localhost:8080/index.html")
    console.log("------------------------------")
});

// ---------------------------------

// WEBSOCKETS

//Create new websocket-server object and connect it to http-server
wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});

//Eventlistener for wsServer request
wsServer.on('request', function(request) {
    connection = request.accept("echo-protocol", request.origin)
    console.log((new Date()) + ' WebSocket connection accepted. ');

    connection.on('message', function(message) {

        var receivedCommand = message.utf8Data
        console.log(receivedCommand)
        var cmd = receivedCommand.split(' ')[0]

        // Basic command (doUpd, doAdd, doRem)
        var commandArray = (message.utf8Data).split(':')
        var cmd=commandArray[0]

        var receivedData = commandArray.slice(1, commandArray.length)

        switch(cmd) {

            case "doUpd": {
                updateTable();
                break;
            }

            case "doAdd": {
                addToTable(receivedData);
                break;
            }

            case "doRem": {
                removeLine(receivedData);
                break;
            }

            case "doFlush": {
                flushTable();
                break;
            }

            default: {
                console.log((new Date()) + ' Something funky happened.')
                break;
            }
        }

    })
})

//Eventlistener for wsServer close
wsServer.on('close',function(){
        console.log((new Date()) + ' Connection closed.')
})

// Function for updating the table from database file
function updateTable() {
    fs.readFile('../Database/database', function (err, data) {
        if (err) throw err;
        connection.sendUTF(data.toString());
    });
}

// Function for adding an item to database
function addToTable(text) {
  fs.open('../Database/database', 'a', 666, function( e, id ) {
   fs.write( id, "\n" + text, null, 'utf8', function(){
    fs.close(id, function(){

        /* Lisättävä if, joka tarkistaa onko tiedostossa tyhjä rivi
         * ennen tuotteen lisäämistä. */

     console.log((new Date()) + ' Item added to table succesfully.');
    });
   });
  });
}

// Function for removing the database file
function flushTable() {
    fs.rename('../Database/database', '../Database/database.deleted', function (err, data) {
        if (err) throw err;
        fs.openSync('../Database/database', 'w')
            console.log((new Date()) + ' Database flushed.')
    });
}

// Doesn't work properly, missing feature
function removeLine(id) {
    fs.readFile('../Database/database', function (err, data) {
        if (err) throw err;
        console.log("ID to be removed: " + id);

        var text = data.toString()
        var temp = new Array()
        temp = text.split(/\n/g)
        console.log(id)
        console.log(id.toString())

        if ( temp.indexOf(id.toString()) != -1) {
            console.log("L")

            var searchResults = dataArray.indexOf(id)
            console.log(searchResults)
        } else
        console.log("Matching id " + id + " can not be found.")
    });
}
